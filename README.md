# my-shared-api

This project is the implementation of a public shared api which is packaged and deployed in a different module. Liferay automatically connects these beans with the api via the annotations.

In this README file you will find everything needed to build, deploy and start this package as well some information on Blade and the Osgi bundle config file. 

Also here are links to recommended further readings.

## Getting Started

This project was generated with 

	blade create -b maven -t api my-shared-api 

All comments in the code start with a `COMMENT:` so they can easily be found via search function.

### Prerequisites

* Running Liferay 7.1 CE
* Activated Gogo Shell access via command line (add `module.framework.properties.osgi.console=localhost:11311` in your `portal-ext.properties` file)
* Maven 3
* Java 8

### Installing

Build with

	# mvn clean package

Deploy it with

	# telnet 127.0.0.1 11311
	g! install file:///PATH_TO_FILE
	g! start BUNLDE_ID

Replace the variable `PATH_TO_FILE` with the absolute path to the jar file (in target folder of the project) and the `BUNDLE_ID` with the bundle id Gogo Shell returns when the bundle was successfully installed.

### Extended Files

* [MySharedServiceProvider.java](src/main/java/space/manhart/demo/shared/api/MySharedServiceProvider.java) 
* [pom.xml](pom.xml) 

### Blade

Download `LiferayProjectSDK` and run it for the installation of Blade.

Afterwards you can go into your shell and execute Blade commands.

List all supported project templates:

	# blade create -l

For setting the build environment to maven add `-b maven` after the `create command`, eg.

	# blade create -b maven -t theme my-theme-with-maven-build

Replace the variable `PATH_TO_FILE` with the absolute path to the jar file (in target folder of the project) and the `BUNDLE_ID` with the bundle id Gogo Shell returns when the bundle was successfully installed.

### bnd.bnd

This file configures the osgi bundle.

Be aware that you have to export all your public api packages here or they will not be available in other modules. The impl module does not need to export anything, since these are not public api but just the implementation of the OSGI beans. 

Also every bundle should define a unique `Bundle-SymbolicName` so you do not encounter any name clashes.  

## Built With

* [Blade](https://dev.liferay.com/develop/tutorials/-/knowledge_base/7-1/blade-cli) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Liferay](https://dev.liferay.com/) - Portal / runtime container

## Further information

* [Gogo Shell 1](https://dev.liferay.com/develop/reference/-/knowledge_base/7-1/using-the-felix-gogo-shell)
* [Gogo Shell 2](https://community.liferay.com/blogs/-/blogs/unleash-the-power-of-gogo-shell)

## Contributing

No contributing since this is a sample project.

## Versioning

No versioning since this is a sample project.

## Authors

* **Manuel Manhart** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

