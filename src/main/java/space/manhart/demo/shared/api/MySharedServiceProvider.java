package space.manhart.demo.shared.api;

import aQute.bnd.annotation.ProviderType;

// COMMENT: changes start
/**
 * This interface defines the public api. The @ProviderType is needed for
 * Liferay to automatically find the implementation classes. (see pom.xml for
 * the required dependency).
 * 
 * 
 * @author Manuel Manhart
 */
@ProviderType
public interface MySharedServiceProvider {

	/**
	 * A sample function which will greet the given name in the log files and the
	 * command line (if output is visible, eg. in Gogo Shell).<br/>
	 * Calls {@link MySharedServiceProvider#getGreeting(String)} for the greeting.
	 * 
	 * @param name the username to greet
	 * @see MySharedServiceProvider#getGreeting(String)
	 */
	public void greet(String name);

	/**
	 * A sample function which will return the greeting for further use.
	 * 
	 * @param name the username to greet
	 * @return the greeting as {@link String}
	 */
	public String getGreeting(String name);

}
// COMMENT: changes end
